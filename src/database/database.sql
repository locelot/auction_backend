DROP TABLE IF EXISTS User_Account;
DROP TABLE IF EXISTS User_Address;
DROP TABLE IF EXISTS User_Info;

CREATE TABLE User_Account(
    id serial PRIMARY KEY,
    userName varchar(30) NOT NULL,
    role varchar(255) NOT NULL,
    email varchar(30) NOT NULL,
    password varchar(255) NOT NULL,
    phoneNumber  varchar(30) NOT NULL,
    secretQuestion varchar(30),
    secretAnswer varchar(30)
);

CREATE TABLE User_Address(
    id SERIAL PRIMARY KEY, 
    city varchar(50),
    street varchar(50),
    postCode varchar(255)
);

CREATE TABLE User_Info(
    id SERIAL PRIMARY KEY,
    userId INTEGER REFERENCES User_Account (id), 
    firstName varchar(50),
    surName varchar(50),
    addressId INTEGER REFERENCES User_Address (id),
    language varchar(50)
);
