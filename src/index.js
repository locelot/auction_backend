const express = require("express");
const cors = require("cors");

const app = express();
const port = process.env.port || 8080;

app.use(cors());
app.get("/", (req, res) => {
  res.json({
    mes: "Hello Server",
  });
});

app.post("/test", (req, res) => {
  res.json({
    mes: "I am post method",
  });
});

app.listen(port, () => {
  console.log(`Server is running on port: ${port}`);
});
