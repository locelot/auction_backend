FROM node:17.6.0

WORKDIR /usr/src/backend

COPY ["package.json", "package-lock.json", "./"]

RUN npm set-script prepare '' && npm ci --only=production && npm cache clean -f

COPY ./ ./

EXPOSE 8080

CMD ["node", "src/index.js"]